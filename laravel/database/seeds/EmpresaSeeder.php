<?php

use Illuminate\Database\Seeder;

class EmpresaSeeder extends Seeder
{
    public function run()
    {
        DB::table('empresa')->insert([
            'texto' => '',
            'imagem_principal' => '',
            'ilustracao_1' => '',
            'ilustracao_2' => '',
            'ilustracao_3' => '',
            'ilustracao_4' => '',
        ]);
    }
}
