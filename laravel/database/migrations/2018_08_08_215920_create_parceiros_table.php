<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParceirosTable extends Migration
{
    public function up()
    {
        Schema::create('parceiros', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('nome');
            $table->string('imagem');
            $table->text('endereco');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('parceiros');
    }
}
