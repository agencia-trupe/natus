<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresaTable extends Migration
{
    public function up()
    {
        Schema::create('empresa', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto');
            $table->string('imagem_principal');
            $table->string('ilustracao_1');
            $table->string('ilustracao_2');
            $table->string('ilustracao_3');
            $table->string('ilustracao_4');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('empresa');
    }
}
