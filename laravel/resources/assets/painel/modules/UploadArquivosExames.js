export default function() {
    $('.btn-adicionar-arquivo').each(function() {
        var errors;

        $(this).fileupload({
            dataType: 'json',
            url: $(this).data('url'),
            formData: { '_method': 'POST' },
            start: function(e, data) {
                errors = [];
                $(this).addClass('loading');
            },
            done: function(e, data) {
                $('.table-arquivos tbody').append(`
                    <tr>
                        <td>
                            <a href="${data.result.path}" target="_blank">${data.result.fileName}</a>
                            <input type="hidden" name="arquivos[]" value="${data.result.fileName}">
                        </td>
                        <td>
                            <a href="#" class="btn btn-danger btn-sm btn-delete-arquivo">
                                <span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>
                                Excluir
                            </a>
                        </td>
                    </tr>
                `).parent().removeClass('hidden');
            },
            stop: function() {
                if (errors.length) {
                    alert(errors.join('\n'));
                }
                $(this).removeClass('loading');
            },
            fail: function(e, data) {
                console.log(data);
                var errorMessage = 'Erro interno do servidor.',
                    response     = data.files[0].name + ' - Erro: ' + errorMessage;

                errors.push(response);
            }
        }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');
    });

    $('body').on('click', '.btn-delete-arquivo', function(event) {
        event.preventDefault();

        var form    = $(this).closest('form'),
            _this   = this;

        bootbox.confirm({
            size: 'small',
            backdrop: true,
            message: 'Deseja excluir o arquivo?',
            buttons: {
                'cancel': {
                    label: 'Cancelar',
                    className: 'btn-default btn-sm'
                },
                'confirm': {
                    label: '<span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir',
                    className: 'btn-primary btn-danger btn-sm'
                }
            },
            callback: function(result) {
                if (result) {
                    $(_this).parent().parent().remove();

                    if (!$('.table-arquivos tbody tr').length) {
                        $('.table-arquivos').addClass('hidden');
                    }
                }
            }
        });
    });
}
