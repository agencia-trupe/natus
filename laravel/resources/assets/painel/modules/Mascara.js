export default function Mascara() {
    Inputmask.extendDefaults({
        jitMasking: true
    });

    $('.mask-cpf').inputmask('999.999.999-99');
};
