import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';

AjaxSetup();
MobileToggle();

$('.banners').cycle({
    slides: '>.banner'
});

$('.exame-toggle').click(function(e) {
    e.preventDefault();
    $(this).next().toggle().parent().toggleClass('open');
});

$('.datepicker').datepicker({
    autoHide: true,
    language: 'pt-BR'
});

$('.area-do-cliente-table tr').click(function() {
    window.location = $(this).data('href');
});
