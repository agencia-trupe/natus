@extends('frontend.common.template')

@section('imagem-circulo')

    <img src="{{ asset('assets/img/layout/agendamento.png') }}" alt="" class="imagem-circulo">

@endsection

@section('side-content')

    <div class="agendamento">
        <div class="texto">
            <h2>AGENDAMENTO</h2>
            <p>Contate a Natus Diagnósticos:</p>
        </div>

        <div class="agendamento-contatos">
            <div>
                <img src="{{ asset('assets/img/layout/icone-tel.png') }}" alt="">
                <span>TELEFONE</span>
                <span>{{ $contato->telefone }}</span>
            </div>
            <div>
                <img src="{{ asset('assets/img/layout/icone-whats.png') }}" alt="">
                <span>WHATSAPP</span>
                <span>{{ $contato->whatsapp }}</span>
            </div>
            <div>
                <img src="{{ asset('assets/img/layout/icone-chat.png') }}" alt="">
                <span>CHAT</span>
                <span><a href="#">Acesse aqui &raquo;</a></span>
            </div>
        </div>

        <form action="{{ route('agendamento.post') }}" method="POST" class="agendamento-form">
            {{ csrf_field() }}

            <h3>SOLICITE SEU AGENDAMENTO ONLINE</h3>

            @if(session('enviado'))
            <div class="enviado">
                <p>Sua solicitação de agendamento foi enviada.</p>
                <p>Entraremos em contato com a confirmação de data e horário.</p>
                <p>GRATOS</p>
            </div>
            @else
            @if($errors->any())
            <div class="erros">
                @foreach($errors->all() as $error)
                {!! $error !!}<br>
                @endforeach
            </div>
            @endif
            <div class="wrapper-col">
                <div class="col">
                    <input type="text" name="nome" placeholder="nome" value="{{ old('nome') }}" required>
                    <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                    <input type="text" name="telefone" placeholder="telefone" value="{{ old('telefone') }}" required>
                    <input type="text" name="data" placeholder="data desejada (selecione)" autocomplete="off" class="datepicker" value="{{ old('data') }}" required>
                    <select name="horario" required>
                        <option value="">horário desejado (selecione)</option>
                        @foreach([
                            '08:00', '08:30', '09:00', '09:30', '10:00',
                            '10:30', '11:00', '11:30', '12:00', '12:30',
                            '13:00', '13:30', '14:00', '14:30', '15:00',
                            '15:30', '16:00', '16:30', '17:00', '17:30',
                            '18:00', '18:30', '19:00', '19:30', '20:00',
                        ] as $horario)
                            <option value="{{ $horario }}" @if(old('horario') == $horario) selected @endif>{{ $horario }}</option>
                        @endforeach
                    </select>
                    <textarea name="mensagem" placeholder="mensagem">{{ old('mensagem') }}</textarea>
                </div>

                <div class="col">
                    <p>Exames a serem realizados:</p>
                    @foreach($exames as $exame)
                        <label>
                            <input type="checkbox" name="exames[]" value="{{ $exame->titulo }}" @if(count(old('exames')) && in_array($exame->titulo, old('exames'))) checked @endif>
                            <span>{{ $exame->titulo }}</span>
                        </label>
                    @endforeach
                </div>
            </div>

            <div class="clear">
                <input type="submit" value="ENVIAR">
            </div>
            @endif
        </form>
    </div>

@endsection
