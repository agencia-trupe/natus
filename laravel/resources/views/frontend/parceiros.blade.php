@extends('frontend.common.template')

@section('side-content')

    <div class="parceiros">
        <div class="texto">
            <h2>PARCEIROS</h2>
        </div>
    </div>

@endsection

@section('content')

    <div class="parceiros-thumbs">
        <div class="center">
            @foreach($parceiros as $parceiro)
            <div class="parceiro" style="background-image:url({{ asset('assets/img/parceiros/'.$parceiro->imagem) }})">
                <div class="endereco">{!! $parceiro->endereco !!}</div>
            </div>
            @endforeach
        </div>
    </div>

@endsection
