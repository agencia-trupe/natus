@extends('frontend.common.template')

@section('imagem-circulo')

    <img src="{{ asset('assets/img/exames-imagem/'.$imagem->imagem) }}" alt="" class="imagem-circulo">

@endsection

@section('side-content')

    <div class="exames">
        <div class="texto">
            <h2>NOSSOS EXAMES</h2>
            <p>Confira nossos exames e faça seu agendamento pelo site.</p>
        </div>

        <div class="exames-links">
            @foreach($exames as $exame)
            <div class="exame @if(request()->has('exame') && request('exame') == $exame->id) open @endif">
                <a href="#" class="exame-toggle">
                    &raquo; {{ $exame->titulo }}
                </a>
                <div class="descricao" @if(request()->has('exame') && request('exame') == $exame->id) style="display:block" @endif>
                    {!! $exame->descricao !!}
                </div>
            </div>
            @endforeach
        </div>
    </div>

@endsection
