    <footer>
        <div class="center">
            <div class="links">
                <a href="{{ route('home') }}">&raquo; home</a>
                <a href="{{ route('empresa') }}">&raquo; empresa</a>
                <a href="{{ route('equipe') }}">&raquo; equipe</a>
                <a href="{{ route('exames') }}">&raquo; exames</a>
                <a href="{{ route('parceiros') }}">&raquo; parceiros</a>
                <a href="{{ route('agendamento') }}">&raquo; agendamento</a>
                <a href="{{ route('contato') }}">&raquo; contato</a>
            </div>
            <div class="exames-lista">
                @foreach($listaExames as $exame)
                <a href="{{ route('exames', ['exame' => $exame->id]) }}">
                    &raquo; {{ $exame->titulo }}
                </a>
                @endforeach
            </div>
            <div class="informacoes">
                <p class="telefone">{{ $contato->telefone }}</p>
                <p class="whatsapp">{{ $contato->whatsapp }}</p>
                @if($contato->facebook || $contato->youtube)
                <div class="social">
                    @if($contato->facebook)
                        <a href="{{ $contato->facebook }}" class="facebook" target="_blank">facebook</a>
                    @endif
                    @if($contato->youtube)
                        <a href="{{ $contato->youtube }}" class="youtube" target="_blank">youtube</a>
                    @endif
                </div>
                @endif
                <p class="endereco">
                    {!! $contato->endereco !!}
                </p>
            </div>
            <div class="copyright">
                <img src="{{ asset('assets/img/layout/logo-branco.png') }}" alt="">
                <p>
                    © {{ date('Y') }} {{ $config->nome_do_site }}<br>
                    Todos os direitos reservados.
                </p>
                <p>
                    <a href="http://www.trupe.net" target="_blank">Criação de sites</a>:<br>
                    <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
                </p>
            </div>
        </div>
    </footer>
