@extends('frontend.common.template')

@section('side-content')

    <div class="contato">
        <div class="texto">
            <h2>FALE COM A NATUS</h2>
        </div>

        <form action="{{ route('contato.post') }}" method="POST" id="form-contato">
            {{ csrf_field() }}
            <input type="text" name="nome" placeholder="nome" value="{{ old('nome') }}" required>
            <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
            <input type="text" name="telefone" value="{{ old('telefone') }}" placeholder="telefone">
            <textarea name="mensagem" placeholder="mensagem" required>{{ old('mensagem') }}</textarea>
            <input type="submit" value="ENVIAR">

            @if(session('enviado'))
                <div class="enviado">Mensagem enviada com sucesso!</div>
            @endif
            @if($errors->any())
                <div class="erro">Preencha todos os campos corretamente.</div>
            @endif
        </form>

        <div class="informacoes">
            <p class="telefone">{{ $contato->telefone }}</p>
            <p class="whatsapp">{{ $contato->whatsapp }}</p>
            <p class="endereco">{!! $contato->endereco !!}</p>
        </div>
    </div>

@endsection

@section('content')

    <div class="mapa">
        {!! $contato->google_maps !!}
    </div>

@endsection
