@extends('frontend.common.template')

@section('side-content')

    <div class="banners">
        @foreach($banners as $banner)
        <div class="banner">
            <img src="{{ asset('assets/img/banners/'.$banner->imagem_1) }}" alt="" class="imagem-1">
            <img src="{{ asset('assets/img/banners/'.$banner->imagem_2) }}" alt="" class="imagem-2">
            <div class="textos">
                <h3>{{ $banner->titulo }}</h3>
                <p>{{ $banner->subtitulo }}</p>
            </div>
        </div>
        @endforeach
    </div>

@endsection

@section('content')

    <div class="home">
        <div class="center">
            <a href="{{ route('agendamento') }}" class="chamada-agendamento">
                <h3>FAÇA O SEU AGENDAMENTO</h3>
                <div class="opcoes">
                    <div><span>WHATSAPP</span></div>
                    <div><span>TELEFONE</span></div>
                    <div><span>CHAT</span></div>
                    <div><span>SITE</span></div>
                </div>
            </a>
        </div>

        <div class="nossos-exames">
            <div class="center">
                <div class="titulo">
                    <h3>NOSSOS <br>EXAMES</h3>
                </div>
                <div class="exames-lista-home">
                    @foreach($exames as $exame)
                    <a href="{{ route('exames', ['exame' => $exame->id]) }}">
                        &raquo; {{ $exame->titulo }}
                    </a>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="localizacao">
            <div class="center">
                <div class="box">
                    <div class="informacoes">
                        <h3>NOSSA LOCALIZAÇÃO</h3>
                        <p class="endereco">
                            {!! $contato->endereco !!}
                        </p>
                        <p class="telefone">{{ $contato->telefone }}</p>
                        <p class="whatsapp">{{ $contato->whatsapp }}</p>
                    </div>
                    <img src="{{ asset('assets/img/contato/'.$contato->imagem_home) }}" alt="">
                </div>
            </div>
        </div>
    </div>

@endsection
