@extends('frontend.common.template')

@section('side-content')

    <div class="empresa">
        <img src="{{ asset('assets/img/empresa/'.$empresa->imagem_principal) }}" alt="">
        <div class="texto">
            {!! $empresa->texto !!}
        </div>
    </div>

@endsection

@section('content')

    <div class="empresa-ilustracoes">
        @foreach(range(1, 4) as $i)
            @if($empresa->{'ilustracao_'.$i})
            <img src="{{ asset('assets/img/empresa/'.$empresa->{'ilustracao_'.$i}) }}" alt="">
            @endif
        @endforeach
    </div>

@endsection
