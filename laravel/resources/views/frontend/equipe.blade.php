@extends('frontend.common.template')

@section('side-content')

    <div class="equipe">
        <div class="texto">
            <h2>ESPECIALISTAS RESPONSÁVEIS</h2>
            <p>Excelência no atendimento, uma equipe de profissionais experientes<br> para garantir o melhor atendimento na realização dos exames.</p>
        </div>

        <div class="especialistas">
            @foreach($equipe as $especialista)
            <div class="especialista">
                <img src="{{ asset('assets/img/equipe/'.$especialista->imagem) }}" alt="">
                <div class="descricao">
                    <h3>{{ $especialista->nome }}</h3>
                    <p class="crm">{{ $especialista->crm }}</p>
                    {!! $especialista->texto !!}
                </div>
            </div>
            @endforeach
        </div>
    </div>

@endsection
