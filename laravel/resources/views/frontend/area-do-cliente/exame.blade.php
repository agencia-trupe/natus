@extends('frontend.common.template')

@section('side-content')

    <div class="area-do-cliente">
        <h2>
            RESULTADOS DE EXAMES &middot;
            Olá {{ $cliente->nome }}
        </h2>
        <p>
            Arquivos do exame: {{ $exame->nome }}<br>
            <a href="{{ route('area-do-cliente') }}">
                &larr; voltar
            </a>
        </p>

        <div class="table-wrapper">
        @if(count(json_decode($exame->arquivos)))
            <table class="area-do-cliente-table">
                <thead>
                    <th>Arquivo</th>
                    <th>Download</th>
                </thead>
                @foreach(json_decode($exame->arquivos) as $key => $arquivo)
                    <tr data-href="{{ route('area-do-cliente.download', [$exame->id, $key]) }}">
                        <td>{{ $arquivo }}</td>
                        <td>
                            <span class="btn-download">
                                Download
                            </span>
                        </td>
                    </tr>
                @endforeach
            </table>
        @else
            <p class="nenhum">Nenhum arquivo encontrado.</p>
        @endif
        </div>
    </div>

@endsection
