@extends('frontend.common.template')

@section('side-content')

    <div class="area-do-cliente">
        <h2>
            RESULTADOS DE EXAMES &middot;
            Olá {{ $cliente->nome }}
        </h2>
        <p>Aqui você faz o download de imagens e resultados dos exames realizados.</p>

        <div class="table-wrapper">
        @if(count($exames))
            <table class="area-do-cliente-table">
                <thead>
                    <th>Exame</th>
                    <th>Realizado em</th>
                    <th>Visualizado</th>
                </thead>
                @foreach($exames as $exame)
                    <tr data-href="{{ route('area-do-cliente.exame', $exame->id) }}">
                        <td>{{ $exame->nome }}</td>
                        <td>{{ $exame->data }}</td>
                        <td>
                            <div class="icone-visualizacao @if($exame->visualizado) visualizado @endif"></div>
                        </td>
                    </tr>
                @endforeach
            </table>
        @else
            <p class="nenhum">Nenhum exame encontrado.</p>
        @endif
        </div>
    </div>

@endsection
