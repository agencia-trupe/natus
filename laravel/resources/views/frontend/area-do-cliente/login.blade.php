@extends('frontend.common.template')

@section('side-content')

    <div class="area-do-cliente">
        <h2>RESULTADOS DE EXAMES</h2>

        <form action="{{ route('area-do-cliente.loginPost') }}" class="login-cliente" method="POST">
            {{ csrf_field() }}

            @if(session('error'))
                <div class="login-error">{{ session('error') }}</div>
            @endif

            <input type="text" name="login" placeholder="login" value="{{ old('login') }}" required>
            <input type="password" name="senha" placeholder="senha" required>
            <input type="submit" value="ENTRAR">
        </form>

        <p>Seu login e senha estão disponíveis no protocolo de realização do seu exame.</p>
    </div>

@endsection
