@extends('painel.common.template')

@section('content')

    <a href="{{ route('painel.exames.index') }}" class="btn btn-sm btn-default">&larr; Voltar para Exames</a>

    <legend>
        <h2><small>Exames /</small> Editar Imagem</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.exames-imagem.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.exames-imagem.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
