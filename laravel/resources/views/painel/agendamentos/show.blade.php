@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Agendamentos</h2>
    </legend>

    <div class="form-group">
        <label>Nome</label>
        <div class="well">{{ $agendamento->nome }}</div>
    </div>

    <div class="form-group">
        <label>E-mail</label>
        <div class="well">
            <button class="btn btn-info btn-sm clipboard" data-clipboard-text="{{ $agendamento->email }}" style="margin-right:5px;border:0;transition:background .3s">
                <span class="glyphicon glyphicon-copy"></span>
            </button>
            {{ $agendamento->email }}
        </div>
    </div>

    <div class="form-group">
        <label>Telefone</label>
        <div class="well">{{ $agendamento->telefone }}</div>
    </div>

    <div class="form-group">
        <label>Data</label>
        <div class="well">{{ $agendamento->data }}</div>
    </div>

    <div class="form-group">
        <label>Horário</label>
        <div class="well">{{ $agendamento->horario }}</div>
    </div>

    <div class="form-group">
        <label>Exames</label>
        <div class="well">{!! $agendamento->exames !!}</div>
    </div>

@if($agendamento->mensagem)
    <div class="form-group">
        <label>Mensagem</label>
        <div class="well">{{ $agendamento->mensagem }}</div>
    </div>
@endif

    <a href="{{ route('painel.agendamentos.index') }}" class="btn btn-default btn-voltar">Voltar</a>

@stop
