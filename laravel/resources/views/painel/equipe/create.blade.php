@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Equipe /</small> Adicionar Especialista</h2>
    </legend>

    {!! Form::open(['route' => 'painel.equipe.store', 'files' => true]) !!}

        @include('painel.equipe.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
