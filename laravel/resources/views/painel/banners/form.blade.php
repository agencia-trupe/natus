@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('imagem_1', 'Imagem 1') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/banners/'.$registro->imagem_1) }}" style="display:block; margin-bottom: 10px; max-width: 100%; border-radius: 100%;">
@endif
    {!! Form::file('imagem_1', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem_2', 'Imagem 2') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/banners/'.$registro->imagem_2) }}" style="display:block; margin-bottom: 10px; max-width: 100%; border-radius: 100%;">
@endif
    {!! Form::file('imagem_2', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título (opcional)') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('subtitulo', 'Subtítulo (opcional)') !!}
    {!! Form::text('subtitulo', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.banners.index') }}" class="btn btn-default btn-voltar">Voltar</a>
