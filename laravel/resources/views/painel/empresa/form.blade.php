@include('painel.common.flash')

<div class="row">
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem_principal', 'Imagem Principal') !!}
            @if($registro->imagem_principal)
            <img src="{{ url('assets/img/empresa/'.$registro->imagem_principal) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('imagem_principal', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            {!! Form::label('texto', 'Texto') !!} {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'empresa']) !!}
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-3">
        <div class="well form-group">
            {!! Form::label('ilustracao_1', 'Ilustração 1') !!}
            @if($registro->ilustracao_1)
            <img src="{{ url('assets/img/empresa/'.$registro->ilustracao_1) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('ilustracao_1', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="well form-group">
            {!! Form::label('ilustracao_2', 'Ilustração 2') !!}
            @if($registro->ilustracao_2)
            <img src="{{ url('assets/img/empresa/'.$registro->ilustracao_2) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('ilustracao_2', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="well form-group">
            {!! Form::label('ilustracao_3', 'Ilustração 3') !!}
            @if($registro->ilustracao_3)
            <img src="{{ url('assets/img/empresa/'.$registro->ilustracao_3) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('ilustracao_3', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="well form-group">
            {!! Form::label('ilustracao_4', 'Ilustração 4') !!}
            @if($registro->ilustracao_4)
            <img src="{{ url('assets/img/empresa/'.$registro->ilustracao_4) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            @endif
            {!! Form::file('ilustracao_4', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
