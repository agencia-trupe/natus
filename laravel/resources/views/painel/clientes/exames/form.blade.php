@include('painel.common.flash')

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('data', 'Data de realização') !!}
            {!! Form::text('data', null, ['class' => 'form-control datepicker']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('nome', 'Nome do Exame') !!}
            {!! Form::text('nome', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('arquivos', 'Arquivos', ['style' => 'display:block']) !!}
            <div class="btn-adicionar-arquivo btn btn-success" style="position:relative;overflow:hidden" data-url="{{ route('painel.clientes.exames.upload') }}">
                <span class="glyphicon glyphicon-plus" style="margin-right:10px"></span>
                Adicionar arquivo(s)
                <input type="file" name="arquivo" class="fileupload" style="position:absolute;opacity:0;top:0;right:0;cursor:pointer;width:100%;height:100%;z-index:2;font-size:200px" multiple>
            </div>
            <p class="alert alert-info small" style="display:inline-block; vertical-align:top; margin-bottom: 0; height:45px; padding: 12px 15px; opacity: .8">
                <span class="glyphicon glyphicon-info-sign" style="margin-right:10px;"></span>
                Selecione um ou mais arquivos.
            </p>

            <style>
            .loading {
                opacity: .5;
                cursor: wait;
            }
            .loading input { display: none; }
            </style>
        </div>

        <table class="table table-striped table-bordered table-hover table-info table-arquivos @if(!isset($exame) || !count(json_decode($exame->arquivos))) hidden @endif" style="margin-top:20px">
            <thead>
                <tr>
                    <th style="width:100%">Arquivo</th>
                    <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
                </tr>
            </thead>
            <tbody>
                @if(isset($exame))
                @foreach(json_decode($exame->arquivos) as $arquivo)
                <tr>
                    <td>
                        <a href="{{ asset('downloads/'.$arquivo) }}" target="_blank">{{ $arquivo }}</a>
                    </td>
                    <td>
                        <a href="#" class="btn btn-danger btn-sm btn-delete-arquivo">
                            <span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>
                            Excluir
                        </a>
                        <input type="hidden" name="arquivos[]" value="{{ $arquivo }}">
                    </td>
                </tr>
                @endforeach
                @endif
            </tbody>
        </table>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.clientes.exames.index', $cliente->id) }}" class="btn btn-default btn-voltar">Voltar</a>
