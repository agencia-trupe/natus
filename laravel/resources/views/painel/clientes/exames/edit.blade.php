@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Clientes / {{ $cliente->nome }} /</small> Editar Exame</h2>
    </legend>

    {!! Form::model($exame, [
        'route' => ['painel.clientes.exames.update', $cliente->id, $exame->id],
        'method' => 'patch',
        'files' => true])
    !!}

        @include('painel.clientes.exames.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
