@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <a href="{{ route('painel.clientes.index') }}" class="btn btn-sm btn-default">&larr; Voltar para Clientes</a>

    <legend>
        <h2>
            <small>Clientes / {{ $cliente->nome }} /</small> Exames
            <a href="{{ route('painel.clientes.exames.create', $cliente->id) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Exame</a>
        </h2>
    </legend>

    @if(!count($exames))
    <div class="alert alert-warning" role="alert">Nenhum exame encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info">
        <thead>
            <tr>
                <th>Adicionado em</th>
                <th>Data de realização</th>
                <th>Nome</th>
                <th>Visualizado</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($exames as $exame)
            <tr class="tr-row">
                <td>
                    {{ $exame->created_at->format('d/m/Y') }}
                    @if($exame->expirado())
                    <div class="label label-warning" style="margin-left:5px;">EXPIRADO</div>
                    @endif
                </td>
                <td>{{ $exame->data }}</td>
                <td>{{ $exame->nome }}</td>
                <td>
                    @if($exame->visualizado)
                    <span class="text-success glyphicon glyphicon-ok"></span>
                    @else
                    <span class="text-danger glyphicon glyphicon-remove"></span>
                    @endif
                </td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.clientes.exames.destroy', $cliente->id, $exame->id],
                        'method' => 'delete'
                    ]) !!}
                        <div class="btn-group">
                            <a href="{{ route('painel.clientes.exames.edit', [$cliente->id, $exame->id]) }}" class="btn btn-primary btn-sm pull-left">
                                <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                            </a>
                            <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                        </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
