@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Clientes / {{ $cliente->nome }} /</small> Adicionar Exame</h2>
    </legend>

    {!! Form::open(['route' => [
            'painel.clientes.exames.store', $cliente->id
        ], 'files' => true]) !!}

        @include('painel.clientes.exames.form', ['submitText' => 'Inserir'])
        
    {!! Form::close() !!}

@endsection
