@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Clientes
            <a href="{{ route('painel.clientes.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Cliente</a>
        </h2>
    </legend>

    <form action="{{ route('painel.clientes.index') }}" method="GET">
        <div class="row">
            <div class="col-md-3">
                <input type="text" name="q" placeholder="Buscar..." class="form-control" value="{{ request('q') }}" required>
            </div>
            <button type="submit" class="btn btn-info col-md-1"><span class="glyphicon glyphicon-search"></span></button>
            @if(request('q'))
            <div class="col-md-2">
                <a href="{{ route('painel.clientes.index') }}" class="btn btn-block btn-warning col-md-2">limpar busca</a>
            </div>
            @endif
        </form>
    </div>

    <hr>

    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info">
        <thead>
            <tr>
                <th>Nome</th>
                <th>CPF</th>
                <th>Login</th>
                <th>Exames</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>{{ $registro->nome }}</td>
                <td>{{ $registro->cpf }}</td>
                <td>{{ $registro->login }}</td>
                <td>
                    <a href="{{ route('painel.clientes.exames.index', $registro->id) }}" class="btn btn-info btn-sm">
                        <span class="glyphicon glyphicon-th-list" style="margin-right:10px"></span>
                        Gerenciar
                    </a>
                </td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.clientes.destroy', $registro->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.clientes.edit', $registro->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{ $registros->appends($_GET)->render() }}
    @endif

@endsection
