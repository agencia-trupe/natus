@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('nome', 'Nome') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('cpf', 'CPF') !!}
    {!! Form::text('cpf', null, ['class' => 'form-control mask-cpf']) !!}
</div>

<div class="form-group">
    {!! Form::label('login', 'Login') !!}
    {!! Form::text('login', null, ['class' => 'form-control']) !!}
</div>

@if(isset($registro))
    <div class="form-group">
        {!! Form::label('senha', 'Alterar Senha') !!}
        {!! Form::text('senha', '', ['class' => 'form-control']) !!}
    </div>
@else
    <div class="form-group">
        {!! Form::label('senha', 'Senha') !!}
        {!! Form::text('senha', null, ['class' => 'form-control']) !!}
    </div>
@endif

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.clientes.index') }}" class="btn btn-default btn-voltar">Voltar</a>
