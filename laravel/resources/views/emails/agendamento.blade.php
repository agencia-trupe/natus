<!DOCTYPE html>
<html>
<head>
    <title>[AGENDAMENTO] {{ $config->nome_do_site }}</title>
    <meta charset="utf-8">
</head>
<body>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Nome:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $nome }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>E-mail:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $email }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Telefone:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $telefone }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Data:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $data }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Horário:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $horario }}</span><br>
@if($mensagem)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Mensagem:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $mensagem }}</span><br>
@endif
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Exames:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{!! $exames !!}</span>
</body>
</html>
