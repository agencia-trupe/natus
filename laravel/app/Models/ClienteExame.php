<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ClienteExame extends Model
{
    protected $table = 'clientes_exames';

    protected $guarded = ['id'];

    protected $dates = ['created_at', 'updated_at', 'data'];

    public function setDataAttribute($date)
    {
        $this->attributes['data'] = Carbon::createFromFormat('d/m/Y', $date);
    }

    public function getDataAttribute($date)
    {
        return Carbon::parse($date)->format('d/m/Y');
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('data', 'DESC')->orderBy('id', 'DESC');
    }

    public function scopeAtivos($query)
    {
        return $query->where(\DB::raw('STR_TO_DATE(created_at ,"%Y-%m-%d")'), '>=', Carbon::now()->subMonth()->format('Y-m-d'));
    }

    public function expirado()
    {
        return $this->created_at->format('Y-m-d') < Carbon::now()->subMonth()->format('Y-m-d');
    }
}
