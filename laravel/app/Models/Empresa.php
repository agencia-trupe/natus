<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Empresa extends Model
{
    protected $table = 'empresa';

    protected $guarded = ['id'];

    public static function upload_imagem_principal()
    {
        return CropImage::make('imagem_principal', [
            'width'  => 300,
            'height' => null,
            'path'   => 'assets/img/empresa/'
        ]);
    }

    public static function upload_ilustracao_1()
    {
        return CropImage::make('ilustracao_1', [
            'width'  => 480,
            'height' => 400,
            'path'   => 'assets/img/empresa/'
        ]);
    }

    public static function upload_ilustracao_2()
    {
        return CropImage::make('ilustracao_2', [
            'width'  => 480,
            'height' => 400,
            'path'   => 'assets/img/empresa/'
        ]);
    }

    public static function upload_ilustracao_3()
    {
        return CropImage::make('ilustracao_3', [
            'width'  => 480,
            'height' => 400,
            'path'   => 'assets/img/empresa/'
        ]);
    }

    public static function upload_ilustracao_4()
    {
        return CropImage::make('ilustracao_4', [
            'width'  => 480,
            'height' => 400,
            'path'   => 'assets/img/empresa/'
        ]);
    }

}
