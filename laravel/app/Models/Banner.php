<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Banner extends Model
{
    protected $table = 'banners';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem_1()
    {
        return CropImage::make('imagem_1', [
            'width'  => 350,
            'height' => 350,
            'path'   => 'assets/img/banners/'
        ]);
    }

    public static function upload_imagem_2()
    {
        return CropImage::make('imagem_2', [
            'width'  => 200,
            'height' => 200,
            'path'   => 'assets/img/banners/'
        ]);
    }
}
