<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use App\Helpers\CropImage;

class Cliente extends Model implements AuthenticatableContract 
{
    use Authenticatable;
    
    protected $table = 'clientes';

    protected $guarded = ['id'];

    protected $hidden = ['senha'];

    public function getAuthPassword()
    {
        return $this->senha;
    }

    public function setRememberToken($value) {}

    public function scopeBusca($query, $value)
    {
        return $query
            ->where('nome', 'LIKE', "%${value}%")
            ->orWhere('login', 'LIKE', "%${value}%")
            ->orWhere('cpf', 'LIKE', "%${value}%");
    }

    public function exames()
    {
        return $this->hasMany(ClienteExame::class, 'cliente_id')->ordenados();
    }
}
