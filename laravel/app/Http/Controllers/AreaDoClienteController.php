<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\ClienteExame;

class AreaDoClienteController extends Controller
{
    public function index()
    {
        $cliente = auth('clientes')->user();
        $exames  = $cliente->exames()->ativos()->get();

        return view('frontend.area-do-cliente.index', compact('cliente', 'exames'));
    }

    public function exame(ClienteExame $exame)
    {
        $cliente = auth('clientes')->user();

        if ($exame->cliente_id != auth('clientes')->user()->id || $exame->expirado()) {
            abort('404');
        }

        $exame->update(['visualizado' => true]);

        return view('frontend.area-do-cliente.exame', compact('cliente', 'exame'));
    }

    public function download(ClienteExame $exame, $arquivoIndex)
    {
        if ($exame->cliente_id != auth('clientes')->user()->id) {
            return response('Acesso não autorizado.', 401);
        }

        $arquivos = json_decode($exame->arquivos) ?: [];

        if (!array_key_exists($arquivoIndex, $arquivos)) {
            return response('Arquivo não encontrado.', 404);
        }

        return response()->download(
            public_path('downloads/'.$arquivos[$arquivoIndex])
        );
    }

    public function login()
    {
        return view('frontend.area-do-cliente.login');
    }
}
