<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Parceiro;

class ParceirosController extends Controller
{
    public function index()
    {
        $parceiros = Parceiro::ordenados()->get();

        return view('frontend.parceiros', compact('parceiros'));
    }
}
