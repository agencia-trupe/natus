<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Especialista;

class EquipeController extends Controller
{
    public function index()
    {
        $equipe = Especialista::ordenados()->get();

        return view('frontend.equipe', compact('equipe'));
    }
}
