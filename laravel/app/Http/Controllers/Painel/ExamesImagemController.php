<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ExamesImagemRequest;
use App\Http\Controllers\Controller;

use App\Models\ExamesImagem;

class ExamesImagemController extends Controller
{
    public function index()
    {
        $registro = ExamesImagem::first();

        return view('painel.exames-imagem.edit', compact('registro'));
    }

    public function update(ExamesImagemRequest $request, ExamesImagem $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = ExamesImagem::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.exames-imagem.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
