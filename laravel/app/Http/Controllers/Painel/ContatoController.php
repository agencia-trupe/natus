<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ContatoRequest;
use App\Http\Controllers\Controller;

use App\Models\Contato;
use App\Helpers\CropImage;

class ContatoController extends Controller
{
    public function index()
    {
        $contato = Contato::first();

        return view('painel.contato.index', compact('contato'));
    }

    public function update(ContatoRequest $request, Contato $contato)
    {
        try {

            $input = $request->all();

            if ($request->file('imagem_home')) {
                $input['imagem_home'] = CropImage::make('imagem_home', [
                    'path'   => 'assets/img/contato/',
                    'width'  => 340,
                    'height' => 340
                ]);
            }

            $contato->update($input);
            return redirect()->route('painel.contato.index')->with('success', 'Informações alteradas com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar informações: '.$e->getMessage()]);

        }
    }
}
