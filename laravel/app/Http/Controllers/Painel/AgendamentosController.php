<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Agendamento;

class AgendamentosController extends Controller
{
    public function index()
    {
        $agendamentos = Agendamento::orderBy('created_at', 'DESC')->get();

        return view('painel.agendamentos.index', compact('agendamentos'));
    }

    public function show(Agendamento $agendamento)
    {
        $agendamento->update(['lido' => 1]);

        return view('painel.agendamentos.show', compact('agendamento'));
    }

    public function destroy(Agendamento $agendamento)
    {
        try {

            $agendamento->delete();
            return redirect()->route('painel.agendamentos.index')->with('success', 'Agendamento excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir agendamento: '.$e->getMessage()]);

        }
    }

    public function toggle(Agendamento $agendamento, Request $request)
    {
        try {

            $agendamento->update([
                'lido' => !$agendamento->lido
            ]);

            return redirect()->route('painel.agendamentos.index')->with('success', 'Agendamento alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar agendamento: '.$e->getMessage()]);

        }
    }
}
