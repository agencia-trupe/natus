<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ClientesExamesRequest;
use App\Http\Controllers\Controller;

use App\Models\Cliente;
use App\Models\ClienteExame;

use App\Helpers\Tools;

class ClientesExamesController extends Controller
{
    public function index(Cliente $cliente)
    {
        $exames = $cliente->exames;

        return view('painel.clientes.exames.index', compact('cliente', 'exames'));
    }

    public function create(Cliente $cliente)
    {
        return view('painel.clientes.exames.create', compact('cliente'));
    }

    public function store(Cliente $cliente, ClientesExamesRequest $request)
    {
        try {
            $request = request()->all();
            $request['arquivos'] = json_encode(request('arquivos') ?: []);

            $cliente->exames()->create($request);

            return redirect()->route('painel.clientes.exames.index', $cliente->id)->with('success', 'Exame adicionado com sucesso.');
        } catch (\Exception $e) {
            return back()->withInput()->withErrors(['Erro ao adicionar exame: '.$e->getMessage()]);
        }

        return back();
    }

    public function edit(Cliente $cliente, ClienteExame $exame)
    {
        return view('painel.clientes.exames.edit', compact('cliente', 'exame'));
    }

    public function update(ClientesExamesRequest $request, Cliente $cliente, ClienteExame $exame)
    {
        try {
            $request = request()->all();
            $request['arquivos'] = json_encode(request('arquivos') ?: []);

            $exame->update($request);

            return redirect()->route('painel.clientes.exames.index', $cliente->id)->with('success', 'Exame alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar exame: '.$e->getMessage()]);

        }
    }

    public function destroy(Cliente $cliente, ClienteExame $exame)
    {
        try {
            $exame->delete();

            return redirect()->route('painel.clientes.exames.index', $cliente->id)->with('success', 'Exame excluído com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao excluir exame: '.$e->getMessage()]);
        }
    }

    public function uploadArquivo()
    {
        try {
            $fileName = Tools::fileUpload('arquivo', 'downloads/');

            return response()->json([
                'fileName' => $fileName,
                'path'     => asset('downloads/'.$fileName)
            ]);
        } catch (\Exception $e) {
            return 'Erro ao adicionar arquivo: '.$e->getMessage();
        }
    }
}
