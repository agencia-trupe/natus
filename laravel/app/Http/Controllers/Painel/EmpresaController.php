<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\EmpresaRequest;
use App\Http\Controllers\Controller;

use App\Models\Empresa;

class EmpresaController extends Controller
{
    public function index()
    {
        $registro = Empresa::first();

        return view('painel.empresa.edit', compact('registro'));
    }

    public function update(EmpresaRequest $request, Empresa $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem_principal'])) $input['imagem_principal'] = Empresa::upload_imagem_principal();
            if (isset($input['ilustracao_1'])) $input['ilustracao_1'] = Empresa::upload_ilustracao_1();
            if (isset($input['ilustracao_2'])) $input['ilustracao_2'] = Empresa::upload_ilustracao_2();
            if (isset($input['ilustracao_3'])) $input['ilustracao_3'] = Empresa::upload_ilustracao_3();
            if (isset($input['ilustracao_4'])) $input['ilustracao_4'] = Empresa::upload_ilustracao_4();

            $registro->update($input);

            return redirect()->route('painel.empresa.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
