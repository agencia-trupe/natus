<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ExamesRequest;
use App\Http\Controllers\Controller;

use App\Models\Exame;

class ExamesController extends Controller
{
    public function index()
    {
        $registros = Exame::ordenados()->get();

        return view('painel.exames.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.exames.create');
    }

    public function store(ExamesRequest $request)
    {
        try {

            $input = $request->all();

            Exame::create($input);

            return redirect()->route('painel.exames.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Exame $registro)
    {
        return view('painel.exames.edit', compact('registro'));
    }

    public function update(ExamesRequest $request, Exame $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.exames.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Exame $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.exames.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
