<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ClientesRequest;
use App\Http\Controllers\Controller;

use App\Models\Cliente;

class ClientesController extends Controller
{
    public function index(Request $request)
    {
        $registros = new Cliente;

        if ($termo = $request->get('q')) {
            $registros = $registros->busca($termo);
        }

        $registros = $registros->latest()->paginate(20);

        return view('painel.clientes.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.clientes.create');
    }

    public function store(ClientesRequest $request)
    {
        try {

            $input = $request->all();

            $input['senha'] = bcrypt($input['senha']);

            Cliente::create($input);

            return redirect()->route('painel.clientes.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Cliente $registro)
    {
        return view('painel.clientes.edit', compact('registro'));
    }

    public function update(ClientesRequest $request, Cliente $registro)
    {
        try {

            $input = array_filter($request->all(), 'strlen');
            
            if (isset($input['senha'])) {
                $input['senha'] = bcrypt($input['senha']);
            }

            $registro->update($input);

            return redirect()->route('painel.clientes.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Cliente $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.clientes.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
