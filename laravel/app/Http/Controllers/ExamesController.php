<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Exame;
use App\Models\ExamesImagem;

class ExamesController extends Controller
{
    public function index()
    {
        $exames = Exame::ordenados()->get();
        $imagem = ExamesImagem::first();

        return view('frontend.exames', compact('exames', 'imagem'));
    }
}
