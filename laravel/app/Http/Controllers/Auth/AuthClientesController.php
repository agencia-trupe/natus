<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;

class AuthClientesController extends Controller
{
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $guard = 'clientes';

    protected $redirectTo = '/area-do-cliente';

    public function __construct()
    {
        $this->middleware('guest.cliente', ['except' => 'logout']);
    }

    protected function login(Request $request)
    {
        if (Auth::guard('clientes')->attempt([
            'login'    => $request->input('login'),
            'password' => $request->input('senha')
        ], false)) {
            return redirect()->route('area-do-cliente');
        }
        else {
            return redirect()->route('area-do-cliente.login')->withInput()->with('error', 'Login ou senha inválidos');
        }
    }
}
