<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Mail;
use App\Http\Requests\AgendamentosRequest;

use App\Models\Contato;
use App\Models\Exame;
use App\Models\Agendamento;

class AgendamentoController extends Controller
{
    public function index()
    {
        $contato = Contato::first();
        $exames  = Exame::ordenados()->get();

        return view('frontend.agendamento', compact('contato', 'exames'));
    }

    public function post(AgendamentosRequest $request, Agendamento $agendamento)
    {
        $data = $request->all();
        $data['exames'] = join(request('exames'), '<br>');

        $agendamento->create($data);
        $this->sendMail($data);

        return redirect('agendamento')->with('enviado', true);
    }

    private function sendMail($data)
    {
        if (! $email = Contato::first()->email) {
           return false;
        }

        Mail::send('emails.agendamento', $data, function($m) use ($email, $data)
        {
            $m->to($email, config('site')->nome_do_site)
               ->subject('[AGENDAMENTO] '.config('site')->nome_do_site)
               ->replyTo($data['email'], $data['nome']);
        });
    }
}
