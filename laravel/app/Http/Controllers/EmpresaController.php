<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Empresa;

class EmpresaController extends Controller
{
    public function index()
    {
        $empresa = Empresa::first();

        return view('frontend.empresa', compact('empresa'));
    }
}
