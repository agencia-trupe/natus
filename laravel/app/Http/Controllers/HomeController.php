<?php

namespace App\Http\Controllers;

use App\Models\Exame;
use App\Models\Contato;
use App\Models\Banner;

class HomeController extends Controller
{
    public function index()
    {
        $banners = Banner::ordenados()->get();
        $exames  = Exame::ordenados()->get();
        $contato = Contato::first();

        return view('frontend.home', compact('banners', 'exames', 'contato'));
    }
}
