<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AuthenticateCliente
{
    public function handle($request, Closure $next, $guard = 'clientes')
    {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('area-do-cliente/login');
            }
        }

        return $next($request);
    }
}
