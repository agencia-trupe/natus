<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('empresa', 'EmpresaController@index')->name('empresa');
    Route::get('equipe', 'EquipeController@index')->name('equipe');
    Route::get('exames', 'ExamesController@index')->name('exames');
    Route::get('parceiros', 'ParceirosController@index')->name('parceiros');
    Route::get('agendamento', 'AgendamentoController@index')->name('agendamento');
    Route::post('agendamento', 'AgendamentoController@post')->name('agendamento.post');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contato.post');

    // Área do Cliente
    Route::group([
        'prefix' => 'area-do-cliente'
    ], function() {
        Route::get('login', 'AreaDoClienteController@login')->name('area-do-cliente.login')->middleware('guest.cliente');
        Route::post('login', 'Auth\AuthClientesController@login')->name('area-do-cliente.loginPost')->middleware('guest.cliente');
        Route::get('logout', 'Auth\AuthClientesController@logout')->name('area-do-cliente.logout');
        Route::get('/', 'AreaDoClienteController@index')->name('area-do-cliente')->middleware('auth.cliente');
        Route::get('exame/{clientes_exames}', 'AreaDoClienteController@exame')->name('area-do-cliente.exame');
        Route::get('exame/{clientes_exames}/download/{arquivo_key}', 'AreaDoClienteController@download')->name('area-do-cliente.download')->middleware('auth.cliente');
    });

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
        Route::get('agendamentos/{agendamentos}/toggle', [
            'as' => 'painel.agendamentos.toggle',
            'uses' => 'AgendamentosController@toggle'
        ]);
        Route::resource('agendamentos', 'AgendamentosController');
		Route::resource('parceiros', 'ParceirosController');
		Route::resource('exames-imagem', 'ExamesImagemController', ['only' => ['index', 'update']]);
		Route::resource('exames', 'ExamesController');
		Route::resource('equipe', 'EquipeController');
		Route::resource('empresa', 'EmpresaController', ['only' => ['index', 'update']]);
		Route::resource('banners', 'BannersController');
        Route::resource('clientes', 'ClientesController');
        Route::resource('clientes.exames', 'ClientesExamesController', [
            'parameters' => ['exames' => 'clientes_exames'],
        ]);
        Route::post('clientes/exames/upload', 'ClientesExamesController@uploadArquivo')->name('painel.clientes.exames.upload');
		Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);

        Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::get('generator', 'GeneratorController@index')->name('generator.index');
        Route::post('generator', 'GeneratorController@submit')->name('generator.submit');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
