<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EmpresaRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'texto' => 'required',
            'imagem_principal' => 'image',
            'ilustracao_1' => 'image',
            'ilustracao_2' => 'image',
            'ilustracao_3' => 'image',
            'ilustracao_4' => 'image',
        ];
    }
}
