<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AgendamentosRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome'     => 'required',
            'email'    => 'required|email',
            'telefone' => 'required',
            'data'     => 'required|date_format:"d/m/Y"',
            'horario'  => 'required',
            'exames'   => 'array|required',
        ];
    }

    public function messages()
    {
        return [
            'nome.required'     => 'Preencha seu nome',
            'email.required'    => 'Preencha seu e-mail',
            'email.email'       => 'Insira um endereço de e-mail válido',
            'telefone.required' => 'Preencha seu telefone',
            'data.required'     => 'Selecione uma data',
            'data.date_format'  => 'Data inválida',
            'horario.required'  => 'Selecione um horário',
            'exames.array'      => 'Selecione ao menos um exame',
            'exames.required'   => 'Selecione ao menos um exame',
        ];
    }
}
