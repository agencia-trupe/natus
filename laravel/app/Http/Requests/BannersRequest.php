<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BannersRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'imagem_1' => 'required|image',
            'imagem_2' => 'required|image',
            'titulo' => '',
            'subtitulo' => '',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem_1'] = 'image';
            $rules['imagem_2'] = 'image';
        }

        return $rules;
    }
}
