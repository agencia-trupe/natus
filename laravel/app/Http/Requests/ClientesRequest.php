<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ClientesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'nome' => 'required|unique:clientes',
            'cpf' => 'required|unique:clientes',
            'login' => 'required|unique:clientes',
            'senha' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['nome'] = 'required|unique:clientes,nome,'.$this->route('clientes')->id;
            $rules['cpf'] = 'required|unique:clientes,cpf,'.$this->route('clientes')->id;
            $rules['login'] = 'required|unique:clientes,login,'.$this->route('clientes')->id;
            $rules['senha'] = '';
        }

        return $rules;
    }
}
