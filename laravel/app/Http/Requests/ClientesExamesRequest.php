<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ClientesExamesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'data' => 'required|regex:/^\d{2}\/\d{2}\/\d{4}$/',
            'nome' => 'required',
        ];
    }
}
